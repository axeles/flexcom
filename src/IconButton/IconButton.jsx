import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles} from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    fontFamily: 'inherit',
    width: 32,
    height: 32,
    cursor: 'pointer',
    textAlign: 'right',
    '&:hover': {
      transform: 'scale(1.05)'
    },
    '& img': {
      width: 28,
      height: 28
    },
    '& svg': {
      width: 28,
      height: 28,
      fill: theme.palette.primary.main,
    }
  },
  svgIcon: {
    fill: theme.palette.primary.main
  }
});

class IconButton extends Component {

  onButtonClick = () => {
    this.props.onButtonClick(this.props.params.buttonCode);
  };

  render() {
    const { classes, params } = this.props;
    return (
      <div
        className={classes && classes.root}
        onClick={this.onButtonClick}
      >
        <picture>
          <source type="image/svg+xml" srcSet={params.iconPath} />
        </picture>
        {/*<svg className={classes && classes.svgIcon} >*/}
          {/*<use xlinkHref={params.iconPath} />*/}
        {/*</svg>*/}
        {/*<img src={params.iconPath} alt='Icon button' />*/}
      </div>
    );
  }
}

IconButton.propTypes = {
  params: PropTypes.shape({
    buttonCode: PropTypes.string.isRequired,
    iconPath: PropTypes.string.isRequired,
  }).isRequired,
  onButtonClick: PropTypes.func.isRequired
};

export default withStyles(styles)(IconButton);
