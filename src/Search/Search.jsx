import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { withStyles, createMuiTheme } from '@material-ui/core/styles';

let theme = createMuiTheme();

const styles = theme => ({
  container: {
    color: '#333',
    fontFamily: 'inherit'
  },
  formHeader: {
    fontSize: '1.2rem',
    fontFamily: 'inherit',
    position: 'relative',
    textTransform: 'uppercase',
    marginBottom: '1.5rem'
  },
  blockIcon: {
    position: 'absolute',
    top: 0,
    left: '-2rem',
    width: '1.4rem',
    height: '1.4rem',
    '& img': {
      width: '100%'
    }
  },
  formHelper: {
    fontSize: '.8rem'
  },
  formGroup: {
  },
  formControl: {
    margin: 0,
  },
  selectField: {
    marginBottom: '1rem',
    width: '10rem'
  },
  searchForm: {
    marginTop: '2rem'
  },
  textField: {
    marginBottom: '1rem',
    width: '10rem'
  },
  submitButton: {
    marginTop: '2rem',
  },
  errorMessage: {
    color: theme.palette.secondary.main,
    fontSize: '1rem',
    marginTop: '.3rem',
    marginBottom: '.8rem'
  }
});

class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dateFrom: this.props.params && this.props.params.dateFrom || '',
      dateTo: this.props.params && this.props.params.dateTo || '',
      uploadedBy: this.props.params && this.props.params.uploadedBy || '',
      status: this.props.params && this.props.params.status || '',
      approvedBy: this.props.params && this.props.params.approvedBy || '',
      isPeriodValid: true
    };

    // console.log('+++ [Search component] props:', this.props);

    this.t = (props.i18n && props.i18n.t instanceof Function ) ? props.i18n.t.bind(props.i18n) : () => false;
  }

  onSubmit = (event) => {
    const {dateFrom, dateTo, uploadedBy, status, approvedBy} = this.state;
    event.preventDefault();
    this.props.onSearch({dateFrom, dateTo, uploadedBy, status, approvedBy});
  };

  onDateFromChanged = (event) => {
    this.setState({ dateFrom: event.target.value, isPeriodValid: true  });
  };

  onDateToChanged = (event) => {
    this.setState({ dateTo: event.target.value, isPeriodValid: true });
  };

  onUploadedByChanged = (event) => {
    this.setState({ uploadedBy: event.target.value });
  };

  onStatusChanged = (event) => {
    this.setState({ status: event.target.value });
  };

  onApprovedByChanged = (event) => {
    this.setState({ approvedBy: event.target.value });
  };

  getUploaders = () => {
    const { uploaders } = this.props.settings;
    if(uploaders instanceof Array) {
      return uploaders.map((item, index) => <MenuItem key={index} value={item}>{item}</MenuItem>)
    }
  };

  getStatuses = () => {
    const { statuses } = this.props.settings;
    if(statuses instanceof Array) {
      return statuses.map((item, index) => <MenuItem key={index} value={item}>{item}</MenuItem>)
    }
  };

  getApprovers = () => {
    const { approvers } = this.props.settings;
    if(approvers instanceof Array) {
      return approvers.map((item, index) => <MenuItem key={index} value={item}>{item}</MenuItem>)
    }
  };

  validatePeriod = () => {
    const { dateTo, dateFrom } = this.state;
    if( dateTo && dateFrom && (dateTo < dateFrom) ) {
      this.setState({isPeriodValid: false});
    } else {
      this.setState({isPeriodValid: true});
    }
  };


  render() {

    const { classes, settings } = this.props;
    const { dateFrom, dateTo, uploadedBy, status, approvedBy, isPeriodValid } = this.state;
    const row = settings && settings.row;
    return (
      <div className={classes && classes.container}>

        <div className={classes && classes.formHeader}>{this.t('search')}
          <div className={classes && classes.blockIcon}>
            <img src={settings.blockIcon} alt=""/>
          </div>
        </div>
        <div className={classes && classes.formHelper}>
          {this.t('searchFormHelperText')}
        </div>
        <form
          name='searchForm'
          onSubmit={this.onSubmit}
          className={classes && classes.searchForm}
        >
          <FormGroup row={row} className={classes && classes.formGroup} >
            <FormControl className={classes && classes.formControl}>
              <TextField
                id="dateFrom"
                label={this.t('dateFrom')}
                type="date"
                value={dateFrom}
                error={!isPeriodValid}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={this.onDateFromChanged}
                onBlur={this.validatePeriod}
                helperText={!isPeriodValid && this.t('incorrectPeriod')}
              />
            </FormControl>
            <FormControl className={classes && classes.formControl}>
              <TextField
                id="dateTo"
                label={this.t('dateTo')}
                type="date"
                value={dateTo}
                error={!isPeriodValid}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={this.onDateToChanged}
                onBlur={this.validatePeriod}
                helperText={!isPeriodValid && this.t('incorrectPeriod')}
              />
            </FormControl>
          </FormGroup>
          <FormGroup row={row} className={classes && classes.formGroup} >
            <FormControl className={classes && classes.formControl}>
              <InputLabel htmlFor="uploadedBy">{this.t('uploadedBy')}</InputLabel>
                <Select
                  value={uploadedBy}
                  inputProps={{
                    name: 'uploadedBy',
                    id: 'uploadedBy'
                  }}
                  className={classes && classes.selectField}
                  onChange={this.onUploadedByChanged}
                >
                  {this.getUploaders()}
                </Select>
            </FormControl>
            <FormControl className={classes && classes.formControl}>
              <InputLabel htmlFor="status">{this.t('status')}</InputLabel>
              <Select
                value={status}
                inputProps={{
                  name: 'status',
                  id: 'status'
                }}
                className={classes && classes.selectField}
                onChange={this.onStatusChanged}
              >
                {this.getStatuses()}
              </Select>
            </FormControl>
          </FormGroup>
          <FormGroup row={row} className={classes && classes.formGroup} >
            <FormControl className={classes && classes.formControl}>
              <InputLabel htmlFor="approvedBy">{this.t('approvedBy')}</InputLabel>
              <Select
                value={approvedBy}
                inputProps={{
                  name: 'approvedBy',
                  id: 'approvedBy'
                }}
                className={classes && classes.selectField}
                onChange={this.onApprovedByChanged}
              >
                {this.getApprovers()}
              </Select>
            </FormControl>
          </FormGroup>
          <FormGroup row={true} className={classes && classes.formGroup}>
            <FormControl className={classes && classes.formControl}>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                className={classes && classes.submitButton}
                disabled={!isPeriodValid}
              >
                {this.t('search')}
              </Button>
            </FormControl>
          </FormGroup>
        </form>
      </div>
    );
  }
}

Search.propTypes = {
  onSearch: PropTypes.func.isRequired,
  settings: PropTypes.shape({
    blockIcon: PropTypes.string,
    uploaders: PropTypes.array.isRequired,
    statuses: PropTypes.array.isRequired,
    approvers: PropTypes.array.isRequired,
    row: PropTypes.bool
  }).isRequired,
  params: PropTypes.shape({
    dateFrom: PropTypes.string,
    dateTo: PropTypes.string,
    uploadedBy: PropTypes.string,
    status: PropTypes.string,
    approvedBy: PropTypes.string
  }),
  i18n: PropTypes.object
};

export default withStyles(styles(theme))(Search);
