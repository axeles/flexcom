import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles, createMuiTheme } from '@material-ui/core/styles';
import FormGroup from "@material-ui/core/FormGroup";
import FormControl from "@material-ui/core/FormControl";


const styles = theme => ({
  container: {
    fontFamily: 'inherit',
    marginTop: '3rem'
  },
  formGroup: {
  },
  formControl: {
  },
  textField: {
    marginTop: '1.8rem'
  },
  searchForm: {
    marginTop: '2rem'
  },
  submitButton: {
    marginTop: '3rem',
  },
  errorMessage: {
    color: theme.palette.secondary.main,
    fontSize: '1rem',
    marginTop: '.3rem',
    marginBottom: '.8rem'
  },
  formHeader: {
    fontSize: '2rem',
    fontFamily: 'inherit'
  }
});

let theme = createMuiTheme();

class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
      login: '',
      loginLength: this.props.loginLength || 1,
      password: '',
      passwordLength: this.props.passwordLength || 1,
      isLoginValid: true,
      isLoginEdited: false,
      isPasswordValid: true,
      isPasswordEdited: false,
      isDataValid: false,
      isSubmitted: false
    };
    if(props.theme) {
      theme = props.theme;
    }
    this.t = (props.i18n && props.i18n.t instanceof Function ) ? props.i18n.t.bind(props.i18n) : () => false;
  }

  validateData = () => {
    return this.validateLogin() && this.validatePassword();
  };

  validateLogin = () => {
    const { login, loginLength } = this.state;

    return login.length >= loginLength;
  };

  validatePassword = () => {
    const { password, passwordLength } = this.state;
    return password.length >= passwordLength;
  };

  onSubmit = (e) => {
    const { login, password } = this.state;
    e.preventDefault();

    if(this.validateData()) {
      this.setState({ isDataValid: true, isSubmitted: true, login: '', password: '' });
      this.props.loginUser( {login, password} )
    } else {
      this.setState({ isDataValid: false });
    }
  };

  handleLoginChange = (e) => {
    const { password, loginLength, passwordLength } = this.state;

    if(e.target.value.length >= loginLength && password.length >= passwordLength) {
      this.setState({ login: e.target.value, isLoginValid: true, isDataValid: true})
    } else if(e.target.value.length >= loginLength) {
      this.setState({ login: e.target.value, isLoginValid: true})
    } else {
      this.setState({ login: e.target.value, isDataValid: false})
    }
  };

  handlePasswordChange = (e) => {
    const { login, loginLength, passwordLength } = this.state;

    if(e.target.value.length >= passwordLength && login.length >= loginLength) {
      this.setState({ password: e.target.value, isPasswordValid: true, isDataValid: true})
    } else if(e.target.value.length >= passwordLength) {
      this.setState({ password: e.target.value, isPasswordValid: true})
    } else {
      this.setState({ password: e.target.value, isDataValid: false})
    }
  };

  onBlur = (field) => {
    switch(field) {
      case 'login':
        if(!this.validateLogin()) {
          this.setState({isLoginValid: false, isLoginEdited: true})
        }
        break;
      case 'password':
        if(!this.validatePassword()) {
          this.setState({isPasswordValid: false, isPasswordEdited: true})
        }
        break;
      default:
    }
  };

  render() {
    const {
      login,
      isLoginValid,
      isLoginEdited,
      password,
      isPasswordValid,
      isPasswordEdited,
      isDataValid,
      isSubmitted,
      loginLength,
      passwordLength
    } = this.state;

    const { classes } = this.props;

    let loginHelperText = this.t('loginHelperText') + ' ' + loginLength + '+ ' + this.t('characters');
    if(loginHelperText === 'loginHelperText' || '') {
      loginHelperText = `Length of login must be ${loginLength}+ characters`;
    }

    let passwordHelperText = this.t('passwordHelperText') + ' ' + passwordLength + '+ ' + this.t('characters');
    if(passwordHelperText === 'passwordHelperText' || '') {
      passwordHelperText = `Length of password must be ${passwordLength}+ characters`;
    }

    return (
      <div className={classes && classes.container}>
        <div className={classes && classes.formHeader}>{this.t('greeting')}</div>
        {
          isSubmitted && !isDataValid &&
          <div className={classes && classes.errorMessage}>
            {this.t('register-failure')}
          </div>
        }
        <form
          name="register-form"
          onSubmit={this.onSubmit}
          className={classes && classes.searchForm}
        >
          <FormGroup className={classes && classes.formGroup}>
            <FormControl className={classes && classes.formControl}>
            <TextField
              label={this.t('username')}
              error={!isLoginValid}
              name="login"
              value={login}
              className={classes && classes.textField}
              onChange={this.handleLoginChange}
              onBlur={() => this.onBlur('login')}
              helperText={!isLoginValid && isLoginEdited && loginHelperText}
              autoComplete='on'
            />
            </FormControl>
            <FormControl className={classes && classes.formControl}>
            <TextField
              label={this.t('password')}
              error={!isPasswordValid}
              type="password"
              name="password"
              value={password}
              className={classes && classes.textField}
              onChange={this.handlePasswordChange}
              onBlur={() => this.onBlur('password')}
              helperText={!isPasswordValid && isPasswordEdited && passwordHelperText}
              autoComplete='off'
            />
            </FormControl>
            <FormControl className={classes && classes.formControl}>
            <TextField
              label={this.t('password2')}
              error={!isPasswordValid}
              type="password"
              name="password2"
              value={password2}
              className={classes && classes.textField}
              onChange={this.handlePasswordChange}
              onBlur={() => this.onBlur('password2')}
              helperText={!isPasswordValid && isPasswordEdited && passwordHelperText}
              autoComplete='off'
            />
            </FormControl>
          </FormGroup>
          <FormGroup row={true} className={classes && classes.formGroup}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes && classes.submitButton}
              disabled={!isDataValid || (isDataValid && isSubmitted)}
            >
              {this.t('login')}
            </Button>
          </FormGroup>
        </form>
      </div>
    );
  }
}

Register.propTypes = {
  signInUser: PropTypes.func.isRequired,
  loginLength: PropTypes.number,
  passwordLength: PropTypes.number,
  i18n: PropTypes.object,
  theme: PropTypes.object
};

export default withStyles(styles(theme))(Register);
