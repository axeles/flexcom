import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Logo from '../Logo'
import IconButton from '../IconButton'

const styles = theme => ({
  root: {
    width: '100%',
    fontFamily: 'inherit',
    display: 'grid',
    gridTemplateColumns: '3rem auto',
    gridTemplateRows: '17rem auto',
    gridGap: '1rem 1rem'
  },
  logo: {
    gridColumnStart: '2',
  },
  backButton: {
    gridRowStart: '2',
    justifySelf: 'end',
    '& active': {
      transform: 'scale(0.8)'
    }
  },
  sidebarBody: {
    gridColumnStart: '2',
    gridRowStart: '2',
    gridRowEnd: '-1 '
  }
});

class Sidebar extends Component {

  render() {
    const { classes, children, params } = this.props;
    return (
      <div className={classes && classes.root}>
        <Logo
          classes={{root: classes.logo}}
          params={params.logoParams}
          onLogoClick={this.props.onLogoClick}
        />
        {params.backButtonParams.buttonCode &&
          <IconButton
            classes={{root: classes.backButton}}
            params={params.backButtonParams}
            onButtonClick={this.props.onButtonClick}
          />
        }
        <div className={classes && classes.sidebarBody}>
          {children}
        </div>
      </div>
    );
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.object.isRequired,
  onLogoClick: PropTypes.func.isRequired,
  onButtonClick: PropTypes.func.isRequired,
  params: PropTypes.shape({
    logoParams: PropTypes.object.isRequired,
    backButtonParams: PropTypes.object.isRequired
  }).isRequired
};

export default withStyles(styles)(Sidebar);
