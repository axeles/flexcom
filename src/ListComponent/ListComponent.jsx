import React, { Component } from 'react';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: '30rem',
    width: '100%',
    fontFamily: 'inherit'
  },
  header: {
    textTransform: 'uppercase',
    color: '#000',
    marginTop: 0,
    marginBottom: '1rem',
    fontSize: '24px',
    fontWeight: '500',
    fontFamily: 'Roboto'
  },
  primaryItemStyle: {
    padding: '0',
  },
  primaryTextStyle: {
    paddingTop: '20px',
    fontSize: '20px',
    fontWeight: '500',
    color: '#000'
  },
  secondaryItemStyle: {
    padding: '0'
  },
  secondaryTextStyle: {
    fontSize: '12px',
    color: '#474747',
    padding: '0'
  },
  iconsContainer: {
    width: 40
  },
  blockContainer: {
    paddingLeft: 5
  },
  iconButton: {
    width: 40,
    height: 40,
    cursor: 'pointer',
    borderRadius: '50%',
  },
  clearPadding: {
    padding: '0'
  }
});

class ListComponent extends Component {

  list = [];

  componentWillMount() {
    this.list = this.buildList();
  }

  onButtonClicked = () => {
    const params = {
      buttonCode: this.props.params.buttonCode
    };
    this.props.onButtonClicked(params);
  };

  buildList = () => {
    const { classes } = this.props;

    return this.props.list.map(item => {
      return (
        <div>
          <List key={item.code}>
            <ListItem button classes={{ root: classes.primaryItemStyle }}>
              <ListItemText
                primary={item.name}
                classes={{ primary: classes.primaryTextStyle }}
              />
            </ListItem>
          </List>
          {item.services && item.services.map(el => {
            return (
              <List key={el.code} classes={{ root: classes.clearPadding }}>
                <ListItem button classes={{ root: classes.secondaryItemStyle }}>
                  <ListItemText
                    secondary={el.name}
                    classes={{ secondary: classes.secondaryTextStyle }}
                  />
                </ListItem>
              </List>
            )
            })
          }
        </div>
      )
    });
  };

  render() {
    // console.log('+++ [List props]', this.props);
    this.list = this.buildList();
    const { classes, params } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes && classes.header}>
          {params.header}
        </div>
        {this.list}
      </div>
    );
  }
}

List.propTypes = {
  classes: PropTypes.object.isRequired,
  onButtonClicked: PropTypes.func.isRequired,
  list: PropTypes.array.isRequired,
  params: PropTypes.shape({
    header: PropTypes.string.isRequired,
    buttonCode: PropTypes.string.isRequired,
    buttonIconPath: PropTypes.string,
    headerIconName: PropTypes.string,
  }).isRequired
};

export default withStyles(styles)(ListComponent);
