import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';


library.add(fas);


const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    height: '100%',
    width: '100%',
  },
  links: {
    '& a': {
      textDecoration: 'none',
      textTransform: 'uppercase'
    }
  },
  header: {
    textTransform: 'uppercase',
    color: theme.palette.primary.main,
    marginTop: 0,
    marginBottom: '1rem',
    fontSize: '1.2rem'
  },
  primaryTextStyle: {
    padding: '.3rem 0'
  },
  linkStyle: {

  },
  activeLink: {
    '& span': {
      color: theme.palette.primary.main
    }
  },
  iconsContainer: {
    width: 40
  },
  blockContainer: {
    paddingLeft: 5
  },
  iconButton: {
    width: 40,
    height: 40,
    cursor: 'pointer',
    borderRadius: '50%',
    '&:active': {
    },
    '&:hover': {
      transform: 'scale(1.05)'
    },
    '& img': {
      width: 28,
      padding: 0
    }
  },
  errorMessage: {
    color: theme.palette.secondary.main,
    fontSize: '1rem',
    marginTop: '.3rem',
    marginBottom: '.8rem'
  }
});

class LinksBlock extends Component {

  links = [];

  componentWillMount() {
    this.links = this.buildLinksList();
  }

  onButtonClicked = () => {
    const params = {
      buttonCode: this.props.params.buttonCode
    };
    this.props.onButtonClicked(params);
  };

  handleLinkClick(clickedLink) {
    const linksList = this.props.linksList.map((link) => {
      const newLink = {...link};
      newLink.active = clickedLink.path === link.path;
      return newLink;
    });
    const activeLinks = linksList.filter((link) => link.active === true);
    this.props.onLinkSelected(activeLinks[0]);
    this.setState({linksList});
  }

  buildLinksList = () => {
    const { classes } = this.props;
    const activeLinkStyle = classes ? classes.activeLink : '';
    const linkStyle = classes ? classes.linkStyle : '';
    const itemStyle = classes ? classes.primaryTextStyle : '';

    return this.props.linksList.map(link => {
      const index = this.props.linksList.indexOf(link);
      return (
        <Link to={link.path} onClick={() => this.handleLinkClick(link)} key={link.path}>
          <ListItem button className={itemStyle}>
            <ListItemText
              primary={link.title}
              className={this.props.linksList[index].active ? activeLinkStyle : linkStyle}
            />
          </ListItem>
        </Link>
      )
    });

  };

  render() {
    // console.log('+++ [LinksBlock props]', this.props);
    this.links = this.buildLinksList();
    const { classes, params } = this.props;
    const header = ' ' + (params.header || "[Section's header is empty]");
    return (
      <div className={classes && classes.container}>
        {
          params.buttonCode && params.buttonIconPath &&
          <div className={classes && classes.iconsContainer}>
            <div className={classes && classes.iconButton} onClick={this.onButtonClicked}>
              <img src={params.buttonIconPath} alt={params.buttonCode} />
            </div>
          </div>
        }
        <div className={classes && classes.blockContainer}>
          <div className={classes && classes.header}>
            {params.headerIconName &&
              <FontAwesomeIcon icon={params.headerIconName}/>
            }
            {header}
          </div>
          <List component="nav" className={ classes && classes.links}>
            {this.links}
          </List>
        </div>
      </div>
    );
  }
}

LinksBlock.propTypes = {
  onButtonClicked: PropTypes.func.isRequired,
  onLinkSelected: PropTypes.func.isRequired,
  linksList: PropTypes.array.isRequired,
  params: PropTypes.shape({
    header: PropTypes.string.isRequired,
    buttonCode: PropTypes.string.isRequired,
    buttonIconPath: PropTypes.string,
    headerIconName: PropTypes.string,
  }).isRequired
};

export default withStyles(styles)(LinksBlock);
