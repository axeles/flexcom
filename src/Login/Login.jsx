import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import FormGroup from "@material-ui/core/FormGroup";
import FormControl from "@material-ui/core/FormControl";

const styles = theme => ({
  container: {
    fontFamily: theme.typography.fontFamily
  },
  root: {
    fontFamily: theme.typography.fontFamily,
    marginLeft: '8rem',
    marginTop: '15rem'
  },
  loginForm: {
    marginTop: '3rem',
    maxWidth: '15rem',
    '&:last-child': {
      marginBottom: '2rem'
    },
  },
  formGroup: {
    '&:last-child': {
      marginTop: '1.5rem',
      alignItems: 'center'
    },
  },
  formControl: {
  },
  textField: {
    marginBottom: '1.8rem',
    '&:focus label': {
      color: theme.palette.secondary.main
    },
    '&:hover label': {
      color: theme.palette.secondary.main
    }
  },
  submitButton: {
    width: theme.props.buttonStyle && theme.props.buttonStyle.medium && theme.props.buttonStyle.medium.width,
    height: theme.props.buttonStyle && theme.props.buttonStyle.medium && theme.props.buttonStyle.medium.height,
    borderRadius: theme.props.buttonStyle && theme.props.buttonStyle.borderRadius,
    boxShadow: theme.props.buttonStyle && theme.props.buttonStyle.boxShadow,
    '&:disabled': {
      color: theme.props.buttonStyle && theme.props.buttonStyle.disabled && theme.props.buttonStyle.disabled.color,
      backgroundColor: theme.props.buttonStyle && theme.props.buttonStyle.disabled && theme.props.buttonStyle.disabled.backgroundColor,
      border: theme.props.buttonStyle && theme.props.buttonStyle.disabled && theme.props.buttonStyle.disabled.border,
      borderColor: theme.props.buttonStyle && theme.props.buttonStyle.disabled && theme.props.buttonStyle.disabled.borderColor
    }
  },
  forgotPasswordLink: {
    marginLeft: '3em',
    fontFamily: 'inherit',
    fontSize: '.75em',
    color: theme.props.linkStyle && theme.props.linkStyle.color,
    '&:hover': {
      color: theme.props.linkStyle && theme.props.linkStyle.hoveredLink
    }
  },
  errorMessage: {
    display: 'inline-block',
    padding: '.5rem 2rem',
    backgroundColor: theme.palette.secondary.light,
    color: theme.palette.alerts && theme.palette.alerts.danger,
    fontSize: '1em',
    marginTop: '2rem'
  },
  formHeaderContainer: {
    display: 'grid',
    gridTemplateColumns: 'minmax(min-content, max-content) auto',
    gridGap: '1rem 1rem'
  },
  formHeader: {
    fontSize: theme.typography.title.fontSize,
    fontWeight: theme.typography.title.fontWeight,
    fontFamily: theme.typography.title.fontFamily,
    textTransform: 'uppercase',

  },
  formHeaderDecorator: {
    display: 'inline-block',
    backgroundColor: theme.palette.tertiary && theme.palette.tertiary.contrastText,
    width: 'calc(100% - 2rem)',
    height: '2px',
    alignSelf: 'center',
    marginLeft: '2rem'
  }
});

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      isLoginValid: true,
      isLoginEdited: false,
      isPasswordValid: true,
      isPasswordEdited: false,
      isDataValid: false,
      isSubmitted: false
    };
    this.t = (props.i18n && props.i18n.t instanceof Function ) ? props.i18n.t.bind(props.i18n) : () => false;
    // console.log('+++ [Login] props:', this.props);
  }

  validateData = () => {
    return this.validateLogin() && this.validatePassword();
  };

  validateLogin = () => {
    const { login } = this.state;
    return login.length > 0;
  };

  validatePassword = () => {
    const { password } = this.state;
    return password.length > 0;
  };

  onSubmit = (e) => {
    const { login, password } = this.state;
    e.preventDefault();

    if(this.validateData()) {
      this.setState({ isDataValid: true, isSubmitted: true, login: '', password: '' });
      this.props.loginUser( {login, password} )
    } else {
      this.setState({ isDataValid: false });
    }
  };

  handleLoginChange = (e) => {
    this.setState({ isSubmitted: false, isDataValid: false });
    if(e.target.value.length > 0 && this.validatePassword()) {
      this.setState({ login: e.target.value, isLoginValid: true, isDataValid: true})
    } else if(e.target.value.length > 0) {
      this.setState({ login: e.target.value, isLoginValid: true})
    } else {
      this.setState({ login: e.target.value, isDataValid: false})
    }
  };

  handlePasswordChange = (e) => {
    this.setState({ isSubmitted: false, isDataValid: false });
    if(e.target.value.length > 0 && this.validateLogin()) {
      this.setState({ password: e.target.value, isPasswordValid: true, isDataValid: true})
    } else if(e.target.value.length > 0) {
      this.setState({ password: e.target.value, isPasswordValid: true})
    } else {
      this.setState({ password: e.target.value, isDataValid: false})
    }
  };

  onBlur = (field) => {
    switch(field) {
      case 'login':
        if(!this.validateLogin()) {
          this.setState({isLoginValid: false, isLoginEdited: true})
        }
        break;
      case 'password':
        if(!this.validatePassword()) {
          this.setState({isPasswordValid: false, isPasswordEdited: true})
        }
        break;
      default:
    }
  };

  render() {
    const {
      login,
      isLoginValid,
      isLoginEdited,
      password,
      isPasswordValid,
      isPasswordEdited,
      isDataValid,
      isSubmitted,
    } = this.state;

    const { classes, forgotPasswordLinkPath, onForgotPassword } = this.props;

    let loginHelperText = this.t('loginHelperText');

    let passwordHelperText = this.t('passwordHelperText');

    return (
      <div className={classes && classes.root}>
        <div className={classes && classes.formHeaderContainer}>
          <div className={classes && classes.formHeader}>{this.t('greeting')}</div>
          <div className={classes && classes.formHeaderDecorator}>&nbsp;</div>
        </div>
        {
          this.props.errorMessage &&
          <div className={classes && classes.errorMessage}>
            {this.props.errorMessage}
          </div>
        }
        <form
          name="login-form"
          onSubmit={this.onSubmit}
          className={classes && classes.loginForm}
        >
          <FormGroup className={classes && classes.formGroup}>
            <FormControl className={classes && classes.formControl}>
              <TextField
                label={this.t('username')}
                error={!isLoginValid}
                name="login"
                value={login}
                className={classes && classes.textField}
                onChange={this.handleLoginChange}
                onBlur={() => this.onBlur('login')}
                helperText={!isLoginValid && isLoginEdited && loginHelperText}
                autoComplete='on'
              />
            </FormControl>
            <FormControl className={classes && classes.formControl}>
              <TextField
                label={this.t('password')}
                error={!isPasswordValid}
                type="password"
                name="password"
                value={password}
                className={classes && classes.textField}
                onChange={this.handlePasswordChange}
                onBlur={() => this.onBlur('password')}
                helperText={!isPasswordValid && isPasswordEdited && passwordHelperText}
                autoComplete='off'
              />
            </FormControl>
          </FormGroup>
          <FormGroup row={true} className={classes && classes.formGroup}>
            <Button
              variant="contained"
              color="secondary"
              disableRipple={true}
              disableFocusRipple={true}
              type="submit"
              className={classes && classes.submitButton}
              disabled={!isDataValid || (isDataValid && isSubmitted)}
            >
              {this.t('login')}
            </Button>
            <Link
              className={classes && classes.forgotPasswordLink}
              to={forgotPasswordLinkPath}
              onClick={onForgotPassword}
            >
              {this.t('forgotPassword')}
            </Link>
          </FormGroup>
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
  forgotPasswordLinkPath: PropTypes.string.isRequired,
  onForgotPassword: PropTypes.func.isRequired,
  errorMessage: PropTypes.string
};

export default withStyles(styles)(Login);
