import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    width: 'calc(100% - 1rem)',
    fontFamily: 'inherit',
    gridColumnStart: '2',
    gridRowStart: '2'
  }
});

class Workbench extends Component {

  render() {
    const { classes, children } = this.props;
    return (
      <div className={classes && classes.root}>
        {children}
      </div>
    );
  }
}

Workbench.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.object.isRequired
};

export default withStyles(styles)(Workbench);
