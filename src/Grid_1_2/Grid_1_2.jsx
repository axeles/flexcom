import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    display: 'grid',
    gridTemplateColumns: '33% calc(67% - 1rem)',
    gridGap: '1rem 1rem',
    fontSize: 'inherit',
    fontFamily: 'inherit'
  }
});

class Grid_1_2 extends Component {

  render() {
    const { classes, children } = this.props;

    return (
      <div className={classes && classes.root}>
        {children}
      </div>
    )
  }
}

Grid_1_2.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.array.isRequired
};

export default withStyles(styles)(withRouter(Grid_1_2));
