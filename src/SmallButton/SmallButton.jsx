import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    fontFamily: 'inherit',
  },
  buttonStyle: {
    padding: '3px 4px',
    minWidth: '2rem',
    minHeight: '1.5rem',
    fontSize: '.7rem',
    cursor: 'pointer',
    fontFamily: 'inherit'
  }
});

class SmallButton extends Component {

  render() {
    const { classes, label, onClick } = this.props;
    return (
      <div className={classes && classes.root}>
        <Button
          onClick={onClick}
          variant='outlined'
          size='small'
          classes={{sizeSmall: classes.buttonStyle}}
        >
          {label}
        </Button>
      </div>
    );
  }
}

SmallButton.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

export default withStyles(styles)(SmallButton);
