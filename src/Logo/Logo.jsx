import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    width: '12rem',
    height: '12rem',
    cursor: 'pointer',
    '& img': {
      width: '100%',
      border: 'none'
    }
  }
});

class Logo extends Component {

  render() {
    const { classes, params } = this.props;

    return (
      <div className={classes && classes.root} onClick={this.props.onLogoClick}>
        <img src={params.logoImagePath} alt="Logo" />
      </div>
    );
  }
}

Logo.propTypes = {
  params: PropTypes.shape({
    logoImagePath: PropTypes.string
  }).isRequired,
  onLogoClick: PropTypes.func.isRequired

};

export default withStyles(styles)(Logo);
