import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: '1600px',
    margin: '0 auto',
    fontFamily: 'Roboto, sans-serif',
    position: 'relative'
  }
});

class FlexWrapper extends Component {

  render() {
    const { classes, children } = this.props;
    return (
      <div className={classes && classes.root}>
        {children}
      </div>
    );
  }
}

FlexWrapper.propTypes = {
  children: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FlexWrapper);
